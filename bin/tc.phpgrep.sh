#!/bin/bash

source "`dirname $0`/tc.common.sh"

_usage()
{
    tc.warning "$0 [-i ignore case] [search word] [directory]" && exit 1
}

#---------------------------------------------------
# parsing options
#---------------------------------------------------
directory="."
search=""
ignore="false"
while getopts ":i" opts; do
    case ${opts} in
        i)
            ignore="true"
            ;;
    esac
done
shift $((OPTIND-1))

if [ -n "$1" ]; then search=$1      ; fi
if [ -n "$2" ]; then directory=$2   ; fi

if [ -z "${search}" ] || [ -z "${directory}" ]; then
    _usage
fi

#---------------------------------------------------
# main
#---------------------------------------------------
options="-nr"
tc.info "searching...\n[directory]:${directory} [search word]:${search} [ignore case]:${ignore}\n"
if [ "${ignore}" = "true" ]; then
    options="${options} -i"
fi

for file in `find ${directory} -name "*.php"`
do
    grep ${options} ${search} ${file}
done
