#!/bin/bash

source "`dirname $0`/tc.common.sh"

_usage()
{
    echo "$0 [-d locale directory]" && exit 1
}

#---------------------------------------------------
# parsing options
#---------------------------------------------------
directory=""
while getopts ":d:" opts; do
    case ${opts} in
        d)
            directory=${OPTARG}
            ;;
        *)
            _usage
    esac
done
shift $((OPTIND-1))

if [ -z "${directory}" ] || ! [ -d "${directory}" ]; then
    _usage
fi

#---------------------------------------------------
# main
#---------------------------------------------------
tc.check_command "msgfmt"
for file in `find ${directory} -name "*.po"`
do
    tc.verbose_exec "msgfmt $file -o ${file%.*}.mo"
done
